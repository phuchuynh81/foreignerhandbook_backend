@extends('layouts.app_master_admin')
@section('content')
<section class="content-header">
                    <h1>
                      Cập nhập menu sản phẩm 
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="{{route('admin.menu.index')}}">Menu</a></li>
                        <li class="active">Update</a></li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
          
                        </div>
                        <div class="box-body">
                         <form role="form" action ="" method="POST" enctype="multipart/form-data">
                              @csrf
                           <div class="col-sm-8">
                             <div class="form-group">
                                <label for="name">Name<span class="text-danger">(*)</span></label>
                                <input class="form-control" name="name" type="text" placeholder="Name ..." value="{{$menu->name}}">
                                @if ($errors->first('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                           </div>
                                <div class="col-sm-8">
                                     <div class="form-group ">
                                    <label class="control-label">Menu<b class="col-red">(*)</b></label>
                                    <select name="id_table"class="form-control ">
                                          <option  value="">__Click__</option>
                                          <option {{($menu->id_table == 0 ? "selected='selected'" : "")}} value="0">topmenu</option>
                                          <option {{($menu->id_table == 1 ? "selected='selected'" : "")}} value="1">bottommenu</option>
                                         <!-- <option value="0">Admin</option>
                                         <option value="1">Customer</option> -->
                                    </select>
                                     @if ($errors->first('id_table'))
                                        <span class="text-danger">{{ $errors->first('id_table') }}</span>
                                    @endif
                                </div>
                                </div>
                           <div class="col-sm-8">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Ảnh đại diện</h3>
                                    </div>
                                    <div class="box-body block-images">
                                        <div style="margin-bottom: 10px"> <img src="{{pare_url_file($menu->icons)}}" onerror="this.onerror=null;this.src='/images/no-image.jpg';" alt="" class="img-thumbnail" style="width: 200px;height: 200px;"> </div>
                                        <div style="position:relative;"> <a class="btn btn-primary" href="javascript:;"> Choose File... <input type="file" style="position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:&quot;progid:DXImageTransform.Microsoft.Alpha(Opacity=0)&quot;;opacity:0;background-color:transparent;color:transparent;" name="icons" size="40" class="js-upload"> </a> &nbsp; <span class="label label-info" id="upload-file-info"></span> </div>
                                    </div>
                            </div>
                           <div class="col-sm-4">
                               
                           </div>
                           <div class="col-sm-12">
                            <div class="box-footer text-center">
                                <button class="btn btn-success" type="submit">Lưu dữ liệu</button>
                                <a href="{{route('admin.menu.index')}}" class="btn btn-danger">Quay lại</a>
                           </div>
                           </div>
                    </form>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            Footer
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                </section>
                @stop