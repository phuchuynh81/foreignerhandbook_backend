@extends('layouts.app_master_admin')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Thêm mới user</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{  route('admin.user.create') }}">User</a></li>
            <li class="active"> Create</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
       <div class="row">
         <form role="form" action="" method="POST" enctype="multipart/form-data">
     @csrf
      <div class="col-sm-8">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Thông tin cơ bản</h3>
            </div>
            <div class="box-body">
               <div class="row">
               	 <div class="col-sm-9">
                	<div class="form-group ">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Nhập thông tin user" autocomplete="off" value="{{$user->name ?? old('name')}}">
                    @if ($errors->first('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
                </div>
               </div>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                             <input type="text" name="email" value="" class="form-control" data-type="currency" placeholder="Nhập thông tin email">
                             @if ($errors->first('name'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                             <input type="text" name="password" value="" class="form-control" data-type="currency" placeholder="Nhập thông tin password">
                             @if ($errors->first('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
               <div class="row">
               	<div class="col-sm-9">
               		 <div class="form-group ">
                    <label class="control-label">Level<b class="col-red">(*)</b></label>
                    <select name="level"class="form-control ">
                          <option  value="">__Click__</option>
                          <option  value="0">Admin</option>
                          <option  value="1">Customer</option>
                         <!-- <option value="0">Admin</option>
                         <option value="1">Customer</option> -->
                    </select>
                     @if ($errors->first('level'))
                        <span class="text-danger">{{ $errors->first('level') }}</span>
                    @endif
                </div>
               	</div>
               </div>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone</label>
                             <input type="text" name="phone" value="" class="form-control" data-type="currency" placeholder="Nhập thông tin email">
                             @if ($errors->first('phone'))
                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
        <div class="row">
            <div class="box-header with-border">
                <label for="exampleInputEmail1">Avatar</label>
            </div>
            <div class="box-body block-images">
                <div style="margin-bottom: 10px"> <img src="/images/no-image.jpg" onerror="this.onerror=null;this.src='/images/no-image.jpg';" alt="" class="img-thumbnail" style="width: 200px;height: 200px;"> </div>
                <div style="position:relative;"> <a class="btn btn-primary" href="javascript:;"> Choose File... <input type="file" style="position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:&quot;progid:DXImageTransform.Microsoft.Alpha(Opacity=0)&quot;;opacity:0;background-color:transparent;color:transparent;" name="avatar" size="40" class="js-upload"> </a> &nbsp; <span class="label label-info" id="upload-file-info"></span> </div>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 clearfix">
        <div class="box-footer text-center">
            <a href="{{route('admin.user.index')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i>Cancel</a>
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Thêm mới</button> </div>
    </div>
</form>


<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<!-- {{-- <div class="box-header with-border">
        <h3 class="box-title">Album ảnh</h3>
    </div>
    <div class="box-body">
         <div class="form-group">
            <div class="file-loading">
                <input id="images" type="file" name="file" multiple class="file" data-overwrite-initial="false" data-min-file-count="0">
            </div>
        </div>
    </div> --}} -->

       </div>
            <!-- /.box -->
    </section>
    <!-- /.content -->
@stop