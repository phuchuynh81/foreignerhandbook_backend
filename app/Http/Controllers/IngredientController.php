<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ingredient;
class IngredientController extends Controller
{
    //
    public function getData($id)
    {
    	$table = Ingredient::where('ingredient.id','=',$id)->get();
    	if($table){
    		return $this->respondWithJson($table,$table->count());
    	}
    }
     public function respondWithJson($data,$total)
    {
        return response()->json([
            'message' => 'Successfully',
            'statuscode' => '200',
            'total' => $total,
            'data' => $data,
        ]);
    }
}
