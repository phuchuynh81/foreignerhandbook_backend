<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\postsuser;
class DiaryController extends Controller
{
    //
    public function getData()
    {

    	 $table = PostsUser::select('postsuser.id','postsuser.id_user','postsuser.image','postsuser.content','postsuser.privacy','postsuser.created_at','users.name','users.avatar')->join('users','postsuser.id_user','=','users.id')->orderBy('postsuser.id','desc')->get();
        return $this->respondData($table);
    }
    public function getDataUserPosts(Request $request,$id){
        $table = PostsUser::select('postsuser.id','postsuser.image','postsuser.content','postsuser.privacy','postsuser.created_at','users.name','users.avatar')->join('users','postsuser.id_user','=','users.id')->where('users.id','=',$id)->get();
        return $this->respondData($table);
    }
     
    public function add(Request $request, $id)
    {
     // $newpostuser[id] = (String) (string) Uuid::generate(4);
        $newpostuser['id_user'] = $id;
        $newpostuser['content'] = $request->input('content');
        $newpostuser['image'] = $request->input('image');
        $newpostuser['privacy'] = $request->input('privacy');
        // $filename = '';
     //    if ($request->image) {
     //         //Kiem tra folder ton tai hong khi ban tao duong dan de luu file
     //        if (!file_exists(public_path('images'))) {
     //            mkdir(public_path('images'), 0777, true);
     //        }
     //        // String of all alphanumeric character và generate
     //        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
     //        $uniqueid   = substr(str_shuffle($str_result), 0, 8);
     //        if( $this->validBase64($request->image) ) {
     //            $data2 = $request->image;
     //            if(isset($data2)){
     //                $uniqueid = $uniqueid;
     //                $folderpath=public_path().'/images/user';
                
     //                list($type, $data2) = explode(';', $data2);
     //                list(, $data2)      = explode(',', $data2);
     //                $data2              = base64_decode($data2);
                    
     //                $quickrfq_dir       = $folderpath.'/';
     //                $file               = $quickrfq_dir . $uniqueid . '.png';
     //                $filename           = $uniqueid . '.png';
     //                file_put_contents($file, $data2);
     //            }
     //        } else{
     //            //tra ve ten cua file
     //            $file_exte = $request->image->getClientOriginalExtension();
     //            $filename = date("Ymdhis") . '.' .$file_exte;        
     //            $request->image->move('images/user', $filename);
     //        }
     //    }
     //    if($filename){
     //        $newpostuser['image'] = 'http://192.168.43.144:8080/foreignerhandbook_laravel/public/images/user/'.$filename;
     //    }
        if($newpostuser){
            $data = PostsUser::create($newpostuser);
        }
        return $this->respondData($data);

    }
    public function delete(Request $request, $id){
            $table = PostsUser::find($id);
            if($table){
                $data = $table->delete();
                return $this->respondData($data);
            }
             return response()->json([
            'statusCode' => 400,
            'errorMessage' => 'Dont find data to delete!',
        ], 400);
    }


    public function edit(Request $request, $id)
    {
        $data = PostsUser::find($id);
        if($data){
            $editpostuser['privacy'] = $request->input('privacy');
            $table = $data->update($editpostuser);
            return $this->respondData($table);
        }
    }



     protected function respondData($table)
    {
        return response()->json([
            'message' => 'Successfully',
            'statuscode' => '200',
            'data' => $table,
            'total' => count((array)$table),
        ]);
    }
}
