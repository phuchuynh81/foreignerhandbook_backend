<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //
    protected $table = 'user_role';

    protected $fillable = ['userId','roleId'];
    
    protected $hidden = ['created_at', 'updated_at'];
    // protected $table = 'user_role';
    
    // protected $fillable = [
    //     'userId',
    //     'roleId',
    // ];
    
    // protected $hidden = ['created_at', 'updated_at'];
}
