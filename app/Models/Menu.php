<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $table = 'menu';
    protected $fillable = [
     'id',
     'name',
     'icons',
     'id_table',
     'created_at',
     'created_update'
    ];
   // protected $hidden = ['created_at','updated_at'];
    
}
