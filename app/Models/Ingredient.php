<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    //
    protected $table = 'ingredient';


    protected $fillable = [
    'name',
    'id_menu',
    ];
    //protected $hidden = ['created_at','updated_at'];
}
