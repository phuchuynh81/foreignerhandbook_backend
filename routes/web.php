<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|Au
*/

// Route::get('/', function () {
//     return view('welcome');
// });
include 'route_admin.php';
// Route::group(['prefix'=>'admin-auth','namespace'=>'Admin\Auth'],
//     function(){   
//     Route::get('login', ['as' => 'getLoginAdmin', 'uses' => 'AdminLoginController@getLoginAdmin']);
//     Route::post('login', ['as' => 'postLogin', 'uses' => 'AdminLoginController@postLogin']);
//     Route::get('logout', ['as' => 'getLogout', 'uses' => 'AdminLoginController@getLogoutAdmin']);
//  	// Route::get('login','AdminLoginController@getLoginAdmin')->name('get.login.admin');l
//  	// Route::post('login','AdminLoginController@postLoginAdmin');
//  	// Route::get('logout','AdminLoginController@getLogoutAdmin')->name('get.logout.admin');
//  });
// Route::group(['prefix'=>'api-admin','namespace'=>'Admin','middleware'=>'check_admin_login'], function(){
//  	Route::get('/', function () {
//     return view('admin.index');
//     });
//  	    Route::group(['prefix'=>'category'],function(){
//         Route::get('','AdminCategoryController@index')->name('admin.category.index');
//         Route::get('create','AdminCategoryController@create')->name('admin.category.create');
//         Route::post('create','AdminCategoryController@store');

//         Route::get('update/{id}','AdminCategoryController@edit')->name('admin.category.update');
//         Route::post('update/{id}','AdminCategoryController@update');

//         Route::get('active/{id}','AdminCategoryController@active')->name('admin.category.active');
//         Route::get('hot/{id}','AdminCategoryController@hot')->name('admin.category.hot');
//         Route::get('delete/{id}','AdminCategoryController@delete')->name('admin.category.delete');
//  	});
//  	  Route::group(['prefix' => 'user'], function(){
//             Route::get('','AdminUserController@index')->name('admin.user.index');
//             Route::get('create','AdminUserController@create')->name('admin.user.create');
//             Route::post('create','AdminUserController@store');
            
//             Route::get('update/{id}','AdminUserController@edit')->name('admin.user.update');
//             Route::post('update/{id}','AdminUserController@update');

//             Route::get('delete/{id}','AdminUserController@delete')->name('admin.user.delete');

//         });
//        Route::group(['prefix' => 'product'], function(){
//             Route::get('','AdminProductController@index')->name('admin.product.index');
//             Route::get('create','AdminProductController@create')->name('admin.product.create');
//             Route::post('create','AdminProductController@store');
//             Route::get('active/{id}','AdminProductController@active')->name('admin.product.active');
//             Route::get('update/{id}','AdminProductController@edit')->name('admin.product.update');
//             Route::post('update/{id}','AdminProductController@update');
//             Route::get('hot/{id}','AdminProductController@hot')->name('admin.product.hot');

//             Route::get('delete/{id}','AdminProductController@delete')->name('admin.product.delete');
//         });


//  });