<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
       // $this->call(RoleSeeder::class);
        $data =[
          
           	'name'=>'admin',
           	'email'=>'admin@gmail.com',
           	'phone'=>'0392532170',
           	'password'=> Hash::make('123123'),
            'level'=> '1',
           	'avatar'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTgolBdeaXdt7hZ4G28YiA8shOCg4jkBg08uA&usqp=CAU',
           	'age'=>'1990-12-01',
           	'gender'=>true,
          
           //  [
           // 	'id'=>2,
           // 	'name'=>'customer',
           // 	'email'=>'customer@gmail.com',
           // 	'phone'=>'0392532171',
           // 	'password'=>md5('123123'),
           // 	'avatar'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTgolBdeaXdt7hZ4G28YiA8shOCg4jkBg08uA&usqp=CAU',
           // 	'age'=>'1997-11-01',
           // 	'gender'=>true
           // ]
        ];

        $role_users = [
           [
           	'userId'=>1,'roleId'=>1,
           ],
            [
           	'userId'=>2,'roleId'=>2,
           ],
        ];
       // DB::table('users')->insert($data);
     //   DB::table('user_role')->insert($role_users);
         \DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'phone'=>'0392532170',
            'password'=> Hash::make('123123'),
            'level'=> '1',
            'avatar'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTgolBdeaXdt7hZ4G28YiA8shOCg4jkBg08uA&usqp=CAU',
            'age'=>'1990-12-01',
            'gender'=>true,
        ]);
    }
}
